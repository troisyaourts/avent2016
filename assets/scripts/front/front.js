'use strict';

console.log("Calendrier de l'avent 2016");

Point = function(x,y){
  this.x = x;
  this.y = y;
}

Spect = function(destination, delay){
  this.destination = destination;
  this.delay = delay;

  if(destination.x < 4) this.checkpoint = new Point(0, -1);
  if(destination.x > 4) this.checkpoint = new Point(8.4, -1);
  if(destination.x == 4) this.checkpoint = new Point(Math.random() < 0.5 ? 0 : 8.4, -1);

  if(destination.y % 2 == 0)
    destination.x += 0.5

}

Spect.prototype.getPosition = function(t){
  var self = this;

  if(t < self.delay) return this.checkpoint;

  var velocityY = 1;
  var velocityX = 1.5;
  var position = new Point(this.checkpoint.x, this.checkpoint.y);

  if( (t - this.delay) < this.destination.y * velocityY ){
      position.y = Math.easeOutSine(t - this.delay, this.checkpoint.y, this.destination.y + 1, this.destination.y * velocityY)
      return position;
  }else{
    this.checkpoint.y = this.destination.y
  }

  if( (t - this.delay - this.destination.y*velocityY) < Math.abs(this.destination.x - self.checkpoint.x) * velocityX ){
      position.x = Math.easeInOutQuad(t - this.delay - this.destination.y*velocityY, 
                                  this.checkpoint.x, 
                                  this.destination.x - self.checkpoint.x, 
                                  Math.abs(this.destination.x - self.checkpoint.x) * velocityX)
      return position;
  }else{
    this.checkpoint.x = this.destination.x
  }

  return new Point(this.checkpoint.x, this.checkpoint.y + 0.05);
}

/* CODE */
Avent = function(){
  var self = this;

  this.personnage = null;
  this.fauteuil = null;

  this.seanceBegan = false;

  this.spects = [ new Spect(new Point(2,3), 0), 
                  new Spect(new Point(4,3), 1),
                  
                  new Spect(new Point(5,2), 3),
                  new Spect(new Point(6,2), 3.75), 

                  new Spect(new Point(3,0), 3.75), 

                  new Spect(new Point(3,1), 5), 
                  new Spect(new Point(2,1), 5.75), 

                  new Spect(new Point(1,2), 6.5),

                  new Spect(new Point(4,1), 7.5),
                  new Spect(new Point(4,4), 7.5),

                  new Spect(new Point(4,2), 9),  
                  new Spect(new Point(5,3), 9.5),
                  new Spect(new Point(2,5), 9),

                  new Spect(new Point(4,5), 11.6),  
                  new Spect(new Point(5,5), 11.4),  
                  new Spect(new Point(6,5), 11.2),  
                  new Spect(new Point(7,5), 11.0),   

                  new Spect(new Point(2,4), 13.0),

                  new Spect(new Point(5,0), 13.0),
                  new Spect(new Point(6,0), 13.5),

                  new Spect(new Point(1,4), 16.0),
                  new Spect(new Point(3,2), 16.0),

                  new Spect(new Point(6,1), 16.0),
                  new Spect(new Point(7,1), 16.75),
                  

                  new Spect(new Point(1,0), 22.0),


                ]

  this.cnv = $("#theater")[0];
  this.ctx = this.cnv.getContext("2d");

  this.onResize();

  this.start_stamp = (new Date()).getTime();

  this.loadAssets()
      .then(function(data){
        self.run();
      },function(error){
        console.log("non")
        console.error(error)
      });

}

Avent.prototype.onResize = function(){
  //keep track of all things that need to be resized

  this.cw = Math.floor(this.cnv.clientWidth);
  this.ch = Math.floor(this.cnv.clientHeight );
}

Avent.prototype.loadAssets = function(){
  var self = this;
  
  var total_assets = 0;
  var assets_loaded = 0;

  var serializer = new XMLSerializer();

  var deferred = Q.defer();
  this.allPromises = [];

  var fauteuil_defer = Q.defer();
  this.allPromises.push(fauteuil_defer.promise);

  d3.xml("/img/fauteuil.svg").mimeType("image/svg+xml").get(function(error, xml) {
    if (error) return fauteuil_defer.reject("[asset missing]");
    self.fauteuil_xml = xml.documentElement;
    self.fauteuil = new Image();
    self.fauteuil.src = 'data:image/svg+xml;base64,'+window.btoa(serializer.serializeToString(self.fauteuil_xml));
    fauteuil_defer.resolve(true);
  });

  var personnage_defer = Q.defer();
  this.allPromises.push(personnage_defer.promise);

  d3.xml("/img/spect.svg").mimeType("image/svg+xml").get(function(error, xml) {
    if (error) return personnage_defer.reject("[asset missing]");
    self.personnage_xml = xml.documentElement;
    self.personnage = new Image();
    self.personnage.src = 'data:image/svg+xml;base64,'+window.btoa(serializer.serializeToString(self.personnage_xml));
    personnage_defer.resolve(true);
  });

  Q.all(this.allPromises)
   .then(function () {
      deferred.resolve(true)
    }, function (error) {
      if(error.substring(0,15) == "[asset missing]"){
        console.log(error);
        deferred.reject("<b>Tous les assets n'ont pas pu être chargés.</b><br>Tentez un rafraîchissement de cette page.");  
      }else{
        deferred.reject(error);  
      }
      
  });
  return deferred.promise;

}

Avent.prototype.run = function(){
  var self = this;
  console.log("let's run")
  this.loop();
}

Avent.prototype.render = function(t){
  // if(t < 25){
    this.ctx.clearRect(0, 0, this.cw, this.ch);

    this.spects.sort(function(a,b){
      return a.getPosition(t).y > b.getPosition(t).y
    })
    
    var self = this;
    [7,7,7,7,7,7,7].forEach(function(v,y){
      for(x = 0 ; x < v ; x++){
        if(y % 2) offset = 0
        else offset = 52
        self.ctx.drawImage(self.fauteuil, x*102 + 95 + offset, y*70+30, 100, 113);
      }
      self.spects.forEach(function(s){
        var pos = s.getPosition(t)
        // console.log(t, pos.x, pos.y)
        if(pos.y >= y && pos.y < y+1){
          self.ctx.drawImage(self.personnage, pos.x*102 + 3, pos.y*70, 80, 156);
        }
      })
    });
  // }

  if(t > 22){
    // if(!this.seanceBegan){
      // this.seanceBegan = true;
    this.ctx.save();
      this.ctx.globalCompositeOperation = "multiply";
      this.ctx.fillStyle = "rgba(0,0,0,0.45)";
      this.ctx.rect(0,0, this.cw, this.ch);
      this.ctx.fill();
    this.ctx.restore();
  }
  if(t > 25){
    this.ctx.save();
      this.ctx.globalCompositeOperation = "screen";
      var opacity = (Math.random()*0.15+0.02).toFixed(2);
      this.ctx.fillStyle = "rgba(255,255,255,"+opacity+")";
      this.ctx.beginPath();
      this.ctx.moveTo(this.cw/2 - 20, 0);
      this.ctx.lineTo(10,this.ch);
      this.ctx.lineTo(this.cw-10, this.ch);
      this.ctx.lineTo(this.cw/2 + 20, 0);
      this.ctx.closePath();
      this.ctx.fill();
    this.ctx.restore();
    // }
    
  }
  
  meter.tick();
}

Avent.prototype.loop = function(){
  var self = this;
  var t = ((new Date()).getTime() - this.start_stamp)/1000;
  this.render(t);
  requestAnimFrame(function(){ return self.loop(); });
}


/* MAIN */
var meter = new FPSMeter($("#fpsmeter")[0],{theme: 'dark',heat:  0, graph: 1});
var av = new Avent();

