var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var merge = require('merge-stream');

var sassPaths = [
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src'
];

gulp.task('sass', function() {
  var sassStream = gulp.src('scss/app.scss')
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'compressed' // if css compressed **file size**
    })
      .on('error', $.sass.logError))
    

  var cssStream = gulp.src([
    
    ])

  return merge(sassStream, cssStream)
    .pipe($.concat('avent.css'))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    // .pipe(gulp.dest('css'));
    .pipe(gulp.dest('../web/dist/css'))

});

gulp.task('scripts', function(){
  return gulp.src([
    'bower_components/jquery/dist/jquery.js',
    'bower_components/foundation-sites/dist/foundation.js',
    'bower_components/d3/d3.js',
    'bower_components/fpsmeter/dist/fpsmeter.js',
    'bower_components/q/q.js',
    'scripts/front/*.js'
  ])
    .pipe($.plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe($.concat('avent.js'))
    .pipe(gulp.dest('../web/dist/js'))
    .pipe($.rename({suffix: '.min'}))
    .pipe($.uglify())
    .pipe(gulp.dest('../web/dist/js'))
})


gulp.task('images', function(){
  gulp.src('assets/images/**/*')
    .pipe($.plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }}))
    //.pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: false })))
    .pipe(gulp.dest('../web/img/'));

  gulp.src([])
    .pipe(gulp.dest('../web/dist/css'));

  gulp.src(['bower_components/tinymce/skins/**/*',])
      .pipe(gulp.dest('../web/dist/js/skins'));
});

gulp.task('default', ['scripts', 'sass'], function() {
  gulp.watch(['scss/**/*.scss'], ['sass']);
  gulp.watch(['scripts/front/*.js'], ['scripts']);
});
