<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Calendrier de l'avent 2016 - TroisYaourts</title>
  <link rel="stylesheet" href="/dist/css/avent.css">
</head>
<body>
  <article class="avent-container">
    <div class="row">
      <div class="large-10 columns">
        <canvas width="945" height="510" id="theater"></canvas>
      </div>
      <div class="large-2 columns">
        <nav class="debug-menu">
          <div id="fpsmeter"></div>
        </nav> 
      </div>
    </div>
  </article>
  <script src="/dist/js/avent.js"></script>
</body>
</html>